# Port Scanner
[![Version](https://img.shields.io/badge/Version-0.1.0-blue)](https://gitlab.com/DeveloperC/port-scanner/commits/master)
[![Pipeline Status](https://gitlab.com/DeveloperC/port-scanner/badges/master/pipeline.svg)](https://gitlab.com/DeveloperC/port-scanner/-/commits/master)
[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)
[![License](https://img.shields.io/badge/License-AGPLv3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)


A simple TCP port scanner written in Go.


## Content
 * [Usage](#usage)
    + [Usage - Arguments](#usage-arguments)
 * [Downloading Binary](#downloading-binary)
 * [Compiling](#compiling)
 * [Issues/Feature Requests](#issuesfeature-requests)


## Usage


### Usage - Arguments
```
  -from-port int
    	The inclusive port number to start scanning from. (default 1)
  -host string
    	The host to perform port scanning upon.
  -to-port int
    	The inclusive port number to scan till. (default 1024)
```


## Downloading Binary
Statically linked compiled binaries are available for download.
Visit the releases page at [https://gitlab.com/DeveloperC/port-scanner/-/releases](https://gitlab.com/DeveloperC/port-scanner/-/releases) to see all the releases, the release notes contains links to binary downloads for various architectures.


## Compiling
Checkout the code repository locally, change into the repository's directory and then compile a binary using the Go toolchain.

```
git clone git@gitlab.com:DeveloperC/port-scanner.git
cd port-scanner/
go build
```

The compiled binary is present at `./port-scanner`.


## Issues/Feature Requests
To report an issue or request a new feature use [https://gitlab.com/DeveloperC/port-scann/-/issues](https://gitlab.com/DeveloperC/port-scanner/-/issues).
