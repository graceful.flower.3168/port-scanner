package main

import (
	"flag"
	"fmt"
	"net"
	"os"
)

func main() {
	hostPtr := flag.String("host", "", "The host to perform port scanning upon.")
	fromPort := flag.Int("from-port", 1, "The inclusive port number to start scanning from.")
	toPort := flag.Int("to-port", 1024, "The inclusive port number to scan till.")

	flag.Parse()

	if *fromPort < 0 {
		fmt.Println("The inclusive port number to start scanning from must be a positive integer.")
	}

	if *toPort < 0 {
		fmt.Println("The inclusive port number to scan till must be a positive integer.")
	}

	if *fromPort > *toPort {
		fmt.Println("The from port number is larger than the till port number.")
		os.Exit(1)
	}

	if *hostPtr == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}

	for port := *fromPort; port <= *toPort; port++ {
		address := fmt.Sprintf("%s:%d", *hostPtr, port)

		conn, err := net.Dial("tcp", address)

		if err != nil {
			fmt.Printf("%d Closed/Filtered.\n", port)
		} else {
			fmt.Printf("%d Open.\n", port)
			conn.Close()
		}
	}
}
